import path from 'path';
import CartParser from './CartParser';
import cart from '../samples/cart.json';

let parser;

beforeEach(() => {
  parser = new CartParser();
});

describe('CartParser - unit tests', () => {
  describe('validate()', () => {
    test('returns an empty array on valid input', () => {
      expect(parser.validate('Product name,Price,Quantity\nMollis consequat,9.00,2')).toEqual([]);
    });

    test('returns a correct error with invalid headings', () => {
      expect(parser.validate('Product name,,Quantity')).toEqual([
        {
          type: 'header',
          row: 0,
          column: 1,
          message: 'Expected header to be named "Price" but received .'
        }
      ]);
    });

    test('returns a correct error when row has too few cells', () => {
      expect(parser.validate('Product name,Price,Quantity\nMollis consequat,9.00')).toEqual([
        {
          type: 'row',
          row: 1,
          column: -1,
          message: 'Expected row to have 3 cells but received 2.'
        }
      ]);
    });

    test('returns a correct error when first column in body is not a non-empty string', () => {
      expect(parser.validate('Product name,Price,Quantity\n,9.00,2')).toEqual([
        {
          type: 'cell',
          row: 1,
          column: 0,
          message: 'Expected cell to be a nonempty string but received "".'
        }
      ]);
    });

    test("returns a correct error when second or third cell in a row aren't numbers", () => {
      expect(parser.validate('Product name,Price,Quantity\nMollis consequat,Hello,2')).toEqual([
        {
          type: 'cell',
          row: 1,
          column: 1,
          message: 'Expected cell to be a positive number but received "Hello".'
        }
      ]);

      expect(parser.validate('Product name,Price,Quantity\nMollis consequat,9.00,Goodbye')).toEqual(
        [
          {
            type: 'cell',
            row: 1,
            column: 2,
            message: 'Expected cell to be a positive number but received "Goodbye".'
          }
        ]
      );
    });

    test("returns a correct error when second or third cell in a row aren't positive numbers", () => {
      expect(parser.validate('Product name,Price,Quantity\nMollis consequat,-10,2')).toEqual([
        {
          type: 'cell',
          row: 1,
          column: 1,
          message: 'Expected cell to be a positive number but received "-10".'
        }
      ]);

      expect(parser.validate('Product name,Price,Quantity\nMollis consequat,9.00,-2')).toEqual([
        {
          type: 'cell',
          row: 1,
          column: 2,
          message: 'Expected cell to be a positive number but received "-2".'
        }
      ]);
    });
  });

  describe('parseLine()', () => {
    test('correctly parses a line', () => {
      expect(parser.parseLine('Consectetur adipiscing,28.72,10')).toMatchObject({
        id: expect.any(String),
        name: 'Consectetur adipiscing',
        price: 28.72,
        quantity: 10
      });
    });
  });

  describe('calcTotal()', () => {
    test('calculates total price', () => {
      expect(parser.calcTotal(cart.items)).toBeCloseTo(cart.total);
    });
  });

  describe('parse()', () => {
    test('correctly parses the csv file', () => {
      const validCsv = `Product name,Price,Quantity
				Mollis consequat,9.00,2
				Tvoluptatem,10.32,1
				Scelerisque lacinia,18.90,1
				Consectetur adipiscing,28.72,10
				Condimentum aliquet,13.90,1`;

      jest.spyOn(parser, 'readFile').mockImplementation(() => validCsv);

      expect(parser.parse()).toMatchObject({
        items: [
          {
            name: 'Mollis consequat',
            price: 9,
            quantity: 2
          },
          {
            name: 'Tvoluptatem',
            price: 10.32,
            quantity: 1
          },
          {
            name: 'Scelerisque lacinia',
            price: 18.9,
            quantity: 1
          },
          {
            name: 'Consectetur adipiscing',
            price: 28.72,
            quantity: 10
          },
          {
            name: 'Condimentum aliquet',
            price: 13.9,
            quantity: 1
          }
        ],
        total: 348.32
      });
    });

    test('throws on a invalid input', () => {
      const invalidCsv = `Product name,Quantity
			Mollis consequat,2
			Scelerisque lacinia,1
			Consectetur adipiscing,10
			Condimentum aliquet,1`;

      jest.spyOn(parser, 'readFile').mockImplementation(() => invalidCsv);

      const original = console.error;
      console.error = jest.fn();

      expect(() => {
        parser.parse();
      }).toThrow('Validation failed!');

      console.error = original;
    });
  });
});

describe('CartParser - integration test', () => {
  test('Correctly parses csv from a file', () => {
    expect(parser.parse(path.join(__dirname, '../samples/cart.csv'))).toMatchObject({
      items: [
        {
          name: 'Mollis consequat',
          price: 9,
          quantity: 2
        },
        {
          name: 'Tvoluptatem',
          price: 10.32,
          quantity: 1
        },
        {
          name: 'Scelerisque lacinia',
          price: 18.9,
          quantity: 1
        },
        {
          name: 'Consectetur adipiscing',
          price: 28.72,
          quantity: 10
        },
        {
          name: 'Condimentum aliquet',
          price: 13.9,
          quantity: 1
        }
      ],
      total: 348.32
    });
  });
});
